package �bung;
//Erstellen Sie Objekte vom Typ "Bruch" erstellen

//Geben Sie disem Objekt 2 Werte �bergeben (Z�hler und Nenner)
//Nenner darf nicht null sein

//man soll 2 beliebige Br�che addieren k�nnen und das Ergebnis soll auch ein Bruch sein
//man soll 2 beliebige Br�che mulitplizieren k�nnen und das Ergebnis soll ein Bruch sein

//man soll einen beliebigen Bruch invertieren (vertauschen von Z�hler und Nenner) k�nnen

public class Bruch
{
	private int zaehler; // Wert Zahler erstellt
	private int nenner; // Wert Nenner erstellt

	public Bruch(int a, int b) // Objekt Bruch erh�lt 2 Werte vom Rechenprogramm
	{
		if (b == 0)
		{
			System.out.println("Geben Sie eine andere Zahl ein");
		} else
		{
			zaehler = a; // holt sich den Wert a vom Rechenprogramm und weist ihn dem Z�hler zu
			nenner = b; // holt sich den Wert b vom Rechenprogramm und weist ihn dem Nenner zu
		}
	}

	public int getZaehler() // Methode getZaehler erstellen
	{
		return zaehler; // gibt den Wert des Z�hlers aus

	}

	public int getNenner() // Methode getNenner erstellen
	{
		return nenner; // gibt den Wert des Nenners aus
	}

	public static Bruch bruchAddieren(Bruch a, Bruch b)
	{
		int x = a.zaehler * b.nenner + b.zaehler * a.nenner;
		int y = a.nenner * b.nenner;

		Bruch c = new Bruch(x, y);

		return c;
	}

	public int bruchInvertieren(int a, int b)
	{
		
		a = zaehler;
		b = nenner;
		
		int temp = a;
		b = zaehler;
		temp = nenner;
		
		return a;
	}

}
