

//Erstellen Sie eine Heizung, die die Temperatur regelt
//Ziel ist es, dass sie ausgeschaltet ist
//die Heizung soll sich entweder ein oder ausschalten - je nach Soll-Temperatur
//die Heizung soll heizen, wenn die Ist-Temperatur kleiner ist als die Soll-Temperatur


public class Heizung
{
	public static void main(String[] args)
	{
		boolean heizung; // Heizung erstellen, die ist entweder ein oder aus
		int Tist = 22; // Ist-Temperatur
		int Tsoll = 20; // Soll-Temperatur

		System.out.println("Es hat:" + " " + Tist + " " + "Grad"); // Ausgabe der Ist-Temperatur

		if (Tist < Tsoll) // wenn die Ist-Temperatur kleiner ist als die Soll-Temperatur
		{
			heizung = true; // soll sich die Heizung einschalten

			System.out.println("Es ist nicht warm genug!\nHeizung einschalten!");
		} else // wenn die Ist-Temperatur gr��er ist als die Soll-Temperatur
		{
			heizung = false; // soll sich die Heizung ausschalten

			System.out.println("Heizung wird ausgeschaltet!"); // Ausabe ""
			System.out.println("Es ist warm genug!"); // Ausgabe ""
		}

		if (heizung == true) // wenn sich die Heizung eingeschaltet ist
		{
			for (int i = Tist; i < Tsoll; i++) // soll sie solange heizen, bis die Soll-Temperatur erreicht ist
			{
				System.out.println("Heizung heizt auf"); // Ausgabe ""

				Tist = i + 1; // Temperatur wird um 1 Grad erh�ht

				System.out.println("Es hat:" + " " + Tist + " " + "Grad"); // Ausgabe "" Ist Temperatur nach
																		  // jedem Heizvorgang
			}
			heizung = false; //Heizung wird ausgeschaltet
			System.out.println("Heizung wird ausgeschaltet!\nEs ist warm genug"); // Ausgabe ""			
		}
	}
}
